#
# spec file for package discord
#
# Copyright (c) 2022 SUSE LLC.
# Copyright (c) 2021 SUSE LINUX GmbH, Nuernberg, Germany.
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via http://bugs.opensuse.org/
#


# Require bash for extglob in install section.
%global _buildshell /bin/bash
%global __requires_exclude ^(libffmpeg|libnode).*
%global __provides_exclude ^(libffmpeg|libnode).*

%define _channel_suffix %{nil}
%define _exename Discord
%define _friendlychannel Stable
%define _name discord

Name:           %{_name}-wbundled-electron
Version:        0.0.24
Release:        0
Summary:        Voice and Text Chat for Gamers
License:        SUSE-NonFree
Group:          Productivity/Networking/Instant Messenger
URL:            https://discordapp.com/
#!RemoteAsset
Source0:        https://dl.discordapp.net/apps/linux/%{version}/%{_name}-%{version}.tar.gz
#Source0:        https://discordapp.com/api/download?platform=linux&format=tar.gz
Source2:        discord-symbolic.svg
Source3:        https://raw.githubusercontent.com/flathub/com.discordapp.Discord/master/com.discordapp.Discord.appdata.xml
Source99:       PERMISSION
BuildRequires:  desktop-file-utils
BuildRequires:  hicolor-icon-theme
BuildRequires:  update-desktop-files
BuildRequires:  unzip
BuildRequires:  asar    
BuildRequires:  desktop-file-utils
BuildRequires:  fdupes
BuildRequires:  sed
Requires:       libatomic1
Conflicts:      discord

# Discord seems to contain no native code
BuildArch:      noarch



%description
Discord is a voice and text chat for gamers. The Text chat supports
inline images and videos. Voice chat includes a jitter buffer,
automatic gain control, noise suppression, echo cancellation.
Server-to-client communications are encrypted.

%prep
%setup -q -n Discord

# remove bundled copy of electron
rm %{_exename}
rm chrome-sandbox
rm chrome_100_percent.pak
rm chrome_200_percent.pak
rm icudtl.dat
rm libEGL.so
rm libGLESv2.so
rm libffmpeg.so
rm libvk_swiftshader.so
rm snapshot_blob.bin
rm v8_context_snapshot.bin
rm -rf locales
rm -rf swiftshader

#remove unused scripts
rm postinst.sh

asar e resources/app.asar resources/app
rm resources/app.asar

sed -i 's[XXXPKGNAMEXXX[%{_name}[g' resources/app/app_bootstrap/autoStart/linux.js

# Patch js code to use system electron. Thanks to Arch Linux for patches.
sed -i "s|process.resourcesPath|'%{_datadir}/%{_name}'|" resources/app/app_bootstrap/buildInfo.js

#Patch window class to display correct icon
#jq --compact-output --arg NAME "%{_name}" '.name=$NAME' resources/app/package.json | sponge resources/app/package.json
#sed -i "s/StartupWMClass=discord/StartupWMClass=%{_name}/" %{_name}.desktop

#Remove development garbage
cd %{_builddir}/%{_exename}/resources/app
find -name '*.orig' -type f -print -delete
find -name '*.bak' -type f -print -delete
find -name '*~' -type f -print -delete
find -name '.*.el' -type f -print -delete
find -name '.eslint*' -type f -print -delete
find -name '.editorconfig' -type f -print -delete
find -name '.gitmodules' -type f -print -delete
find -name '.npmignore' -type f -print -delete
find -name '.tm_properties' -type f -print -delete
find -name '.travis.yml' -type f -print -delete
find -name '.yarn-integrity' -type f -print -delete
find -name '.jshintrc' -type f -print -delete
find -name '.github' -print0 |xargs -r0 -- rm -rvf

#Fix file mode
find . -type f -exec chmod 644 {} \;

%autopatch -p1

%build
cd %{_builddir}

#We disable packing of asar, it is counterproductive on linux (makes dupe hardlinking impossible)
asar p %{_builddir}/%{_exename}/resources/app %{_builddir}/%{_exename}/resources/app.asar --unpack '*' --unpack-dir '*'
rm -rf %{_builddir}/%{_exename}/resources/app

#Create starter script.
echo "#!/bin/sh" > %{_builddir}/%{_name}
echo "ELECTRON_ENABLE_STACK_DUMPING=true exec electron %{_datadir}/%{_name}/app.asar \"\$@\"" >> %{_builddir}/%{_name}

%install
install -pDm 755 %{_builddir}/%{_name} %{buildroot}%{_bindir}/%{_name}

cd %{_builddir}/Discord
install -Dm644 discord.png %{buildroot}%{_datadir}/icons/hicolor/256x256/apps/%{_name}.png
desktop-file-install \
  --dir=%{buildroot}%{_datadir}/applications \
  --set-key=Exec \
  --set-value=%{_bindir}/%{_name} \
  %{_name}.desktop

install -Dm644 %{SOURCE2} %{buildroot}%{_datadir}/icons/hicolor/symbolic/apps/%{_name}-symbolic.svg
install -Dm644 %{SOURCE3} %{buildroot}%{_datadir}/appdata/com.discordapp.Discord.appdata.xml

# install share
mkdir -p %{buildroot}%{_datadir}/%{_name}
shopt -s extglob
cp -r !(discord*|*.so) %{buildroot}%{_datadir}/%{_name}
shopt -u extglob

# fix missing icon in some environments
ln -sf %{_datadir}/icons/hicolor/256x256/apps/%{_name}.png %{buildroot}%{_datadir}/%{_name}

%fdupes %{buildroot}%{_prefix}

%post
%if 0%{?suse_version} < 1500
%desktop_database_post
%endif

%if 0%{?suse_version} < 1500 
%postun
%desktop_database_postun
%endif

%files
%defattr(-,root,root)
%{_bindir}/%{_name}
%{_datadir}/applications/%{_name}.desktop
%{_datadir}/icons/hicolor/256x256/apps/%{_name}.png
%{_datadir}/icons/hicolor/symbolic/apps/%{_name}-symbolic.svg
%{_datadir}/appdata/com.discordapp.Discord.appdata.xml
%{_datadir}/%{_name}

%changelog
